import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './style.scss';

const Navigator = ({ activeDay, className }) => {
  const oneDay = 86400000;

  const todayDate = new Date();
  let activeDate = todayDate;

  if (activeDay) {
    const splitActiveDay = activeDay.split('-');
    activeDate = new Date(splitActiveDay[0], (splitActiveDay[1] - 1), splitActiveDay[2]);
  }

  const backwardDate = new Date(activeDate.getTime() - oneDay);
  const forwardDate = new Date(activeDate.getTime() + oneDay);

  const activeSlug = `${activeDate.getFullYear()}-${activeDate.getMonth() + 1}-${activeDate.getDate()}`;
  const todaySlug = `${todayDate.getFullYear()}-${todayDate.getMonth() + 1}-${todayDate.getDate()}`;
  const backwardSlug = `${backwardDate.getFullYear()}-${backwardDate.getMonth() + 1}-${backwardDate.getDate()}`;
  const forwardSlug = `${forwardDate.getFullYear()}-${forwardDate.getMonth() + 1}-${forwardDate.getDate()}`;

  const isToday = activeSlug === todaySlug;

  return (
    <div className={`component-navigator ${className}`}>
      <Link to={`/${backwardSlug}`} className="component-navigator__button">&lt;</Link>
      <Link to={`/${todaySlug}`} className={`component-navigator__button ${isToday ? 'component-navigator__button--disabled' : ''}`}>today</Link>
      <Link to={`/${forwardSlug}`} className={`component-navigator__button ${isToday ? 'component-navigator__button--disabled' : ''}`}>&gt;</Link>
    </div>
  );
};

Navigator.propTypes = {
  activeDay: PropTypes.string,
  className: PropTypes.string
};

Navigator.defaultProps = {
  activeDay: '',
  className: ''
};

export default Navigator;
