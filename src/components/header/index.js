import React from 'react';

import './style.scss';

const Header = () => (
  <header className="component-header">
    <h1>Astronomy Picture of the Day</h1>
  </header>
);

export default Header;
