import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const MediaImage = ({ url, copyright, className }) => {
  return (
    <div className={`component-media-image ${className}`}>
      <figure className="component-media-image__figure">
        <img src={url} alt="Image of the day" className="component-media-image__img" />
        <figcaption className="component-media-image__copyright">
          Copyright: {copyright}
        </figcaption>
      </figure>
    </div>
  );
};

MediaImage.propTypes = {
  copyright: PropTypes.string,
  url: PropTypes.string.isRequired,
  className: PropTypes.string
};

MediaImage.defaultProps = {
  copyright: 'Not informed',
  className: ''
};

export default MediaImage;
