import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const MediaVideo = ({ url, className }) => {
  return (
    <div className={`component-media-video ${className}`}>
      <div className="component-media-video__video-wrapper">
        <iframe src={url} title="apod video" className="component-media-video__video" />
      </div>
    </div>
  );
};

MediaVideo.propTypes = {
  url: PropTypes.string.isRequired,
  className: PropTypes.string
};

MediaVideo.defaultProps = {
  className: ''
};

export default MediaVideo;
