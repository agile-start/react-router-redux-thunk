import React from 'react';
import PropTypes from 'prop-types';

import MediaImage from 'components/media-image';
import MediaVideo from 'components/media-video';

import './style.scss';

const ComponentApod = ({ data, className }) => {
  const {
    copyright, explanation, title, url, media_type: mediaType
  } = data;

  return (
    <div className={`component-apod ${className}`}>
      <h2 className="component-apod__title">{title}</h2>

      {
        mediaType === 'video'
          ? <MediaVideo url={url} copyright={copyright} className="component-apod__media" />
          : <MediaImage url={url} copyright={copyright} className="component-apod__media" />
      }

      <p className="component-apod__explanation">{explanation}</p>

    </div>
  );
};

ComponentApod.propTypes = {
  data: PropTypes.shape({
    copyright: PropTypes.string,
    explanation: PropTypes.string,
    media_type: PropTypes.string,
    title: PropTypes.string,
    url: PropTypes.string
  }).isRequired,
  className: PropTypes.string
};

ComponentApod.defaultProps = {
  className: ''
};

export default ComponentApod;
