import axios from 'axios';

const config = {
  nasaApiHost: 'https://api.nasa.gov/',
  nasaApiKey: ''
};

function setApiKey (key) {
  config.nasaApiKey = key;
}

function apod ({ date }) {
  let apiUrl = `${config.nasaApiHost}planetary/apod?api_key=${config.nasaApiKey}`;

  if (date) apiUrl += `&date=${date}`;

  return axios.get(apiUrl)
    .then((dataRequest) => dataRequest.data);
}

export default {
  apod,
  setApiKey
};

// REACT_APP_NASA_API_KEY=Ee5eP7RcsR1aeY7F00cDJpYVjaq0bCcgovGH6rND
