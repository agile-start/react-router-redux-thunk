import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import Header from 'components/header';
import Footer from 'components/footer';
import Loading from 'components/loading';

import Routes from 'routes';

import './style.scss';

const PageMain = ({ loading }) => {
  return (
    <div className="page-main">
      <Header />
      <div className="page-main__content">
        {
          loading
            ? <Loading />
            : <Routes />
        }
      </div>
      <Footer />
    </div>
  );
};

PageMain.propTypes = {
  loading: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  loading: state.loading
});

export default connect(mapStateToProps)(PageMain);
