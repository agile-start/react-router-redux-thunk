import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { loadApod } from 'store/apod/actions';

import Apod from 'components/apod';
import Navigator from 'components/navigator';

import './style.scss';

const ScreenApod = ({
  match, apodData, apodLoad, apodStatus, className
}) => {
  const { date } = match.params;

  useEffect(() => {
    apodLoad({ date });
  }, [date]);

  return (
    <div className={`screen-apod ${className}`}>
      <Navigator activeDay={date} />
      {
        apodStatus === 'ok'
          ? <Apod data={apodData} />
          : <div className="screen-apod__erro">Content not found for the date</div>
      }
    </div>
  );
};

ScreenApod.propTypes = {
  apodData: PropTypes.object.isRequired,
  apodStatus: PropTypes.string.isRequired,
  apodLoad: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  className: PropTypes.string
};

ScreenApod.defaultProps = {
  className: ''
};

const mapStateToProps = (state) => ({
  apodData: state.apod.media,
  apodStatus: state.apod.status
});

const mapDispatchToProps = (dispatch) => ({
  apodLoad: (params) => dispatch(loadApod(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(ScreenApod);
