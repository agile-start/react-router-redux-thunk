import nasaApi from 'library/nasa-api';
import config from 'config';

import { startLoading, stopLoading } from 'store/loading/actions';

export const UPDATE_APOD_DATE = 'UPDATE_APOD_DATE';
export const UPDATE_APOD_MEDIA = 'UPDATE_APOD_MEDIA';
export const INVALID_APOD_DATE = 'INVALID_APOD_DATE';

nasaApi.setApiKey(config.REACT_APP_NASA_API_KEY);

export function updateApodDate (value) {
  return {
    type: UPDATE_APOD_DATE,
    payload: value.date
  };
}

export function updateApodMedia (value) {
  return {
    type: UPDATE_APOD_MEDIA,
    payload: value
  };
}

export function invalidApodDate () {
  return {
    type: INVALID_APOD_DATE
  };
}

// Load APOD from NASA site.
export function loadApod (params = {}) {
  const { date } = params;

  return (dispatch, getState) => {
    const state = getState().apod;

    if ((!date && state.status === 'unloaded') || (date && state.date !== date)) {
      dispatch(startLoading());

      nasaApi.apod({ date })
        .then((data) => {
          dispatch(updateApodMedia(data));
        })
        .catch(() => {
          dispatch(invalidApodDate());
        })
        .then(() => {
          dispatch(updateApodDate({ date }));
          dispatch(stopLoading());
        });
    }
  };
}
