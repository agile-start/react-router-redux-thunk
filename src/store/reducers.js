import { combineReducers } from 'redux';

import apod from './apod/reducer';
import loading from './loading/reducer';

export default combineReducers({
  apod, loading
});
