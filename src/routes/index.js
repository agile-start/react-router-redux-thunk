import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Apod from 'screens/apod';

const preRender = (Element, { match }) => {
  return (
    <Element match={match} />
  );
};

const routes = [
  {
    key: 'root',
    path: '/',
    exact: true,
    render: (context) => preRender(Apod, context)
  },
  {
    key: 'date',
    path: '/:date',
    exact: true,
    render: (context) => preRender(Apod, context)
  }

];

const Routes = () => {
  return (
    <Switch>
      {
        routes.map((route) => (
          <Route
            key={route.key}
            path={route.path}
            exact={route.exact}
            render={route.render}
          />
        ))
      }
    </Switch>
  );
};

export default Routes;
